import java.util.Scanner;

public class Methods
{
public static void getRequirements()
{
    System.out.println("Temperature Conversion Program\n");
    System.out.println("Devloper: Jordan Straus");
    System.out.println("Program converts user-entered temputures into Fahreneit or Celsius Scales");
    System.out.println("Program countines to prompt for user entry until no longer requested.");
    System.out.println("Note: upper or lower case letters permitted. Though, incorrect entries are not premited");
    System.out.println("Note: Program does not validate numeric data (optional requirment");
    System.out.println();

}

public static void convertTemp()
{
    Scanner sc = new Scanner(System.in);
    double temperature = 0.0;
    char choice = ' ';
    char type = ' ';

    do 
    {
        System.out.print("Fahrenheight to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"c\": ");
        type = sc.next().charAt(0);
        type = Character.toLowerCase(type);
        if(type == 'f')
        {
                System.out.print("Enter Temperature in Fahrenheit: ");
                temperature = sc.nextDouble();
                temperature = ((temperature - 32)*5)/9;
                System.out.println("Temperature in Celsuis = " + temperature);

        }
        else if (type == 'c')
        {
           System.out.print("Enter Temperature in Celsius: ");
           temperature = sc.nextDouble();
           temperature = (temperature * 9/5)+32;
           System.out.println("Temperature in Fahrenheit = " + temperature); 
        }
        else
        {
            System.out.println("Incorrect entry. Please try again.");
        }
        System.out.print("\nDo you want to convert a Temperature (y or n)? ");
        choice = sc.next().charAt(0);
        choice = Character.toLowerCase(choice);
    
    }

        while(choice == 'y');
        System.out.println("\nThank you for using our Temperature Conversion Program!");
    }
}