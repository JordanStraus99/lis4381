> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381- Mobile Web Application Development

## Jordan Straus

### Assignment 4 Requirements:

## Four Parts

1. Clone Assignment
2. Create index.php min 3 Slides
3. Create favicon
4. Skill Sets

#### README.md file should inclue the following items

1. LIS4381 Portal Bootstrap
2. Failed Validation Bootstrap
3. Passed Validation Bootstrap
4. Link to Local web-app
5. Skill Sets 10-12


#### Links To Local Web-App:
1. [Local Web-App:](http://localhost/repos/lis4381/index.php "Online Portfolio") 


#### Assignment 4 Screenshots :

| *Screenshot of A4 Pet Store *:       | *Screenshot of A4 Pet Store Invaild*:   |
| ------------------------------------------------ | -------------------------------------------- |
|![Screenshot of Application](IMG/2s.png)      | ![Screenshot of Application](IMG/1s.png) |


| *Screenshot of  Carosel*:                    |
| ------------------------------------------------ |
|![Online Portfolio:](IMG/3s.png)                   |



| *Screenshot of  Skill Set 10*:                   | *Screenshot of Skills Set 11*:                 |
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Skill Set 10:](IMG/SS10.png)                  | ![Skill Set 11:](IMG/SS11.png)                  |                          



| *Screenshot of  Skill Set 12*:                    |
| ------------------------------------------------ |
|![Skill Set 12:](IMG/SS12.png)                   |








