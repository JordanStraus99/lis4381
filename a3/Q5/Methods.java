import java.util.Random;
import java.util.Scanner;

public class Methods {
    
    public static void getRequirments()
    {
        System.out.println("Devloper Jordan Straus");
        System.out.println("Print minimum and maxium integer values.");
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated intergers (min 1).");
        System.out.println("Use the following loop structures: for, enhanced for, while, do...while.\n");
        
        System.out.println("Integer.MIN_Value = " + Integer. MIN_VALUE);
        System.out.println("Integer.MAX_Value = " + Integer. MAX_VALUE);
        
        System.out.println();
        
    }
    
    public static int[] createArray()
    {
    Scanner scnr = new Scanner(System.in);
    int arraySize = 0;
    
    System.out.print("Enter desired number of pseudorandom integer(min 1):");
    arraySize = scnr.nextInt();
    
    int yourArray[] = new int[arraySize];
    return yourArray;
    }
    
    public static void generatePsedudoRandomNumbers(int [] myArray)
    {
        Random r = new Random();
        int i = 0;
        System.out.println("for loop:");
        for(i=0; i<myArray.length; i++)
        {
            System.out.println(r.nextInt());
        }
    
        System.out.println("\nEnhanced for loop:"); 
        for(int n: myArray)
        {
            System.out.println(r.nextInt());
        }
    
        System.out.println("\nwhile loop:");
        i=0; 
        while(i<myArray.length)
        {
            System.out.println(r.nextInt());
            i++;
        }
        i=0;
        System.out.println("\ndo...while loop:");
        do
        {
            System.out.println(r.nextInt());
            i++;
        }
        while (i < myArray.length);
    }
}
    












