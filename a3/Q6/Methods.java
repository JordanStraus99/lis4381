import java.util.Scanner;

public class Methods 
{
    public static void getRequirements()
    {
        System.out.println("Program prompts user for the first name and age,then print results. ");  
        System.out.println("Create four methods from the following requirments:");
        System.out.println("1. getRequirements(): Void methods displays program requirments.");
        System.out.println("2. getUserInput(): Void Method prompts for user input,\nthen calls two methods: myVoidMethod() and my ValueReturningMethod().");
        System.out.println("3. myVoidMethod():\n" +
         "\ta.accecpts two arguments: String and int.\n" + 
         "\tb.Print user's first name and age.");
        System.out.println("4. myValueReturningMethod():\n" + 
         "\ta. Accepts two arguments: String and int. \n" + 
         "tb. Retunrs String containg first name and age.");

         System.out.println();
    }
    
 public static void getUserInput()
{
    String firstName="";
    int userAge = 0;
    String myStr="";
    Scanner sc = new Scanner(System.in);

    System.out.print("Enter first name: ");
    firstName=sc.next();

    System.out.print("Enter age: ");
    userAge = sc.nextInt();

    System.out.println();
    
    System.out.print("void method call: ");
    myVoidMethod(firstName, userAge);

    System.out.print("value-returning method call: ");
    myStr = myValueReturningMethod(firstName, userAge);
    System.out.println(myStr);

}

    public static void myVoidMethod(String first, int age)
    {
        System.out.println(first + "is" + age);
    }

    public static String myValueReturningMethod(String first, int age)
    {
 
        return first + "is" + age;
    }
}


