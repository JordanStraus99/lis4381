-- MySQL Script generated by MySQL Workbench
-- Tue Oct  6 12:41:20 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema jms18g
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `jms18g` ;

-- -----------------------------------------------------
-- Schema jms18g
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `jms18g` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `jms18g` ;

-- -----------------------------------------------------
-- Table `jms18g`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jms18g`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jms18g`.`petstore` (
  `pst_id` SMALLINT(10) NOT NULL,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` VARCHAR(2) NOT NULL,
  `pst_zip` INT(9) NOT NULL,
  `pst_phone` BIGINT(12) NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(225) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jms18g`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jms18g`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jms18g`.`customer` (
  `cus_id` SMALLINT(10) NOT NULL,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT(15) NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(10,2) NOT NULL,
  `cus_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(225) NULL,
  PRIMARY KEY (`cus_id`),
  UNIQUE INDEX `cus_id_UNIQUE` (`cus_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `jms18g`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jms18g`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `jms18g`.`pet` (
  `pet_id` SMALLINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT(5) NOT NULL,
  `cus_id` SMALLINT(5) NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT(2) NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATETIME NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_nutter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(225) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_customer_idx` (`cus_id` ASC),
  INDEX `fk_pet_petstore1_idx` (`pst_id` ASC),
  UNIQUE INDEX `cus_id_UNIQUE` (`cus_id` ASC),
  UNIQUE INDEX `pst_id_UNIQUE` (`pst_id` ASC),
  CONSTRAINT `fk_pet_customer`
    FOREIGN KEY (`cus_id`)
    REFERENCES `jms18g`.`customer` (`cus_id`)
    ON DELETE Set Null
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `jms18g`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `jms18g`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `jms18g`;
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'Fluffy Friends', '15563 Estuary Drive', 'Boca Raton', 'FL', 33498, 5612517850, 'FluffyFriends@gmail.com', 'https://www.FluffyFriends.com/', 167320.92, 'notes1');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, 'Pups Place', '504 Main Street', 'Delray Beach', 'FL', 33428, 561357969, 'PupsPlace@gmail.com', 'https://www.PupsPlace.com/', 367829.89, 'notes2');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, 'Puupy and Friends', '731 Gaines Street', 'Boca Raton', 'FL', 33498, 561238950, 'PuupyandFriends@gmail.com', 'https://www.PuppyandFriends.com/', 52672.34, 'notes3');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, 'Peace Love Pets', '123 Pensacola Ave', 'Tallahassee', 'FL', 32304, 805747936, 'PeaceLovePets@gmail.com', 'https://www.PeaceLovePets.com/', 2422.53, 'notes4');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, 'Petco', '875 St Augustine Street', 'Miami', 'FL', 03050, 305688261, 'Petco@gmail.com', 'https://www.Petco.com/', 27892.89, 'notes5');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, 'Pet Smart', '982 Skyridge Circle', 'Tallahassee', 'FL', 32304, 805784628, 'PetSmart@gmail.com', 'https://www.PetSmart.com/', 27843.27, 'notes6');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, 'Happy Paws', '647 Tennse Street', 'Miami', 'FL', 03050, 305782684, 'HappyPaws@gmail.com', 'https://www.HappyPaws.com/', 2684.39, 'notes7');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, 'Pet Tree', '555 Maple Stret', 'Delray Beach', 'FL', 33428, 561538458, 'PetTree@gmail.com', 'https://www.PetTree.com/', 3272.23, 'notes8');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, 'Puppy House', '901 Bulbury Street', 'Orlando', 'FL', 55669, 321464930, 'PuppyHouse@gmail.com', 'https://www.PuppyHouse.com/', 27943.88, 'notes9');
INSERT INTO `jms18g`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, 'Kisses and Paws', '232 Abbey Road', 'Orlando', 'FL', 55669, 321684692, 'KissesandPaws@gmail.com', 'https://www.KissesandPaws.com/', 2784.47, 'notes10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `jms18g`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `jms18g`;
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (1, 'Jordan', 'Straus', '123 Main Street', 'Boca Raton', 'FL', 33498, 5612517850, 'jordanstraus@gmail.com', 55.50, 100.99, 'notes1');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (2, 'Marc', 'Jay', '234 Estuary Drive', 'Delray Beach', 'FL', 33428, 561234960, 'marcjay@gmail.com', 100.99, 200.99, 'notes2');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (3, 'Deborah', 'Ferdie', '345 Skyridge Circle', 'Boca Raton', 'FL', 33498, 561946930, 'deborahferdie@gmail.com', 250.55, 300.99, 'notes3');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (4, 'Idan', 'Forrest', '456 FSU WAY', 'Tallhasse', 'FL', 32304, 805784628, 'idanforrest@gmail.com', 500.99, 600.99, 'notes4');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (5, 'Sydney', 'Allen', '567 City Ave', 'Long Island', 'FL', 57869, 805728732, 'sydneyallen@gmail.com', 75.25, 150.99, 'notes5');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (6, 'Lexi', 'White', '999 Beach Cicle', 'Miami', 'FL', 03050, 305729749, 'jayciedickman@gmail.com', 990.89, 2000.99, 'notes6');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (7, 'Jaycie', 'Dickman', '876 Town Way', 'Boca Raton', 'FL', 33498, 561234532, 'lexiwhite@gmail.com', 1234.76, 1550.99, 'notes7');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (8, 'Julia', 'Ornstien', '100 Bridge Stret', 'Long Island', 'FL', 70467, 704729799, 'juliaornstien@gmail.com', 647.99, 750.99, 'notes8');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (9, 'Mackie', 'Arnold', '605 Disney Drive', 'Orlando', 'FL', 57869, 721648903, 'mackiearnold@gmail.com', 688.63, 700.99, 'notes9');
INSERT INTO `jms18g`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_sales`, `cus_notes`) VALUES (10, 'Andrew', 'Hass', '777 Universal Ave', 'Orlando', 'FL', 57869, 721847929, 'andresshass@gmail.com', 234.88, 300.99, 'notes10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `jms18g`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `jms18g`;
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (1, 1, 1, 'Dog', 'f', 200.00, 250.00, 5, 'Brown', '2020-09-02', 'y', 'y', 'notes1');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (2, 2, 2, 'Cat', 'f', 250.00, 200.00, 2, 'Black', '2020-03-16', 'y', 'y', 'notes2');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (3, 3, 3, 'Dog', 'm', 100.00, 150.00, 5, 'Brown', '2020-02-15', 'y', 'y', 'notes3');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (4, 4, 4, 'Cat', 'm', 150.00, 100.00, 1, 'Brown', '2020-06-19', 'y', 'y', 'notes4');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (5, 5, 5, 'Dog', 'f', 300.00, 350.00, 1, 'White', '2020-06-28', 'y', 'y', 'notes5');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (6, 6, 6, 'Cat', 'f', 350.00, 300.00, 2, 'White', NULL, 'y', 'y', 'notes6');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (7, 7, 7, 'Dog', 'm', 500.00, 450.00, 6, 'Brown', '2020-08-22', 'y', 'y', 'notes7');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (8, 8, 8, 'Cat', 'm', 505.00, 455.00, 3, 'Black', '2020-09-16', 'y', 'y', 'notes8');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (9, 9, 9, 'Dog', 'f', 800.00, 750.00, 2, 'Brown', NULL, 'y', 'y', 'notes9');
INSERT INTO `jms18g`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nutter`, `pet_notes`) VALUES (10, 10, 10, 'Cat', 'f', 800.00, 805.00, 2, 'Black', '2020-05-11', 'y', 'y', 'notes10');

COMMIT;

