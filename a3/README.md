> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381- Mobile Web Application Development

## Jordan Straus

### Assignment 3 Requirements:

## Four Parts

1. Concert Ticket Mobile Appp
2. Create a mwb and SQL
3. Chapter Questions
4. Java Skill Sets 4-6

#### README.md file should inclue the following items

1. Screenshots of  running mobile app
2. Screenshots of Java Skill Sets
3. Screenshots of ERD
3. Link of SQL  & mwb


#### Assignment 3 Screenshots :

| *Screenshot of Concert Tickets Modile App*:       | *Screenshot of Concert Tickets Modile App*:   |
| ------------------------------------------------ | -------------------------------------------- |
|![Screenshot of Application](img/tv1.png)      | ![Screenshot of Application](img/tv2.png) |





| *Screenshot of  Skill Set 4*:                   | *Screenshot of Skills Set 5*:                 |
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Skill Set 4:](img/quiz4.png)                  | ![Skill Set 5:](img/quiz5f.JPEG)                  |                          



| *Screenshot of  Skill Set 6*:                    |
| ------------------------------------------------ |
|![Skill Set 6:](img/quiz6.png)                   |




| *Screenshot of  ERD*:                    |
| ------------------------------------------------ |
|![ERD:](img/ERD.png)                   |






#### Links To a3.mwb and a3.sql :
1. [a3.mwb:](docs/a3_1.mwb) 
2. [a3.sql:](docs/a3.sql) 