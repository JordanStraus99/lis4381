> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381- Mobile Web Application Development

## Jordan Straus

### Assignment 2 Requirements:

## Three Parts

1. Healthy Recipe Mobile Appp
2. Chapter Questions(chap3&4)
3. Bitbucket repo link
4. Java Skill Sets 1-3

#### README.md file should inclue the following items

1. Screenshots of running mobile app
2. Screenshots of Java Skill Sets

#### Assignment 2 Screenshots :

| *Screenshot of Healthy Recipe Modile App*:       | *Screenshot of Healthy Recipe Modile App*:   |
| ------------------------------------------------ | -------------------------------------------- |
|![Screenshot of Application](images/SS1.png)      | ![Screenshot of Application](images/SS2.png) |





| *Screenshot of  Skill Set 1*:                   | *Screenshot of Skills Set 2*:                 |
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Skill Set 1:](images/SS5.png)                  | ![Skill Set 2:](images/SS4.png)                  |                          



| *Screenshot of  Skill Set 3*:                    |
| ------------------------------------------------ |
|![Skill Set 3:](images/SS3.png)                   |