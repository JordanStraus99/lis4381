public class ArraysAndLoops
{
    public static void main(final String args[])
    {

        System.out.println("Devloper: Jordan Straus");
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop structure: for, enhances for, while do...while.");
        System.out.println();
        System.out.println("Note: Pretest loops: for, enhanced while. Posttest loop: do..while");
        System.out.println();
        
        //Set up array
        final String[] animals = {"dog", "cat", "bird", "fish", "insect"};
        
        System.out.println("for loop:");
        for (int i=0; i < animals.length; i++){
            System.out.println(animals[i]);
        }
        
        System.out.println();
        
        System.out.println("enhanced for loop:");
        for (final String x : animals) {
            System.out.println(x);
        }
        
        System.out.println();

        System.out.println("while loop:");
        int y = 0;
        while (y < animals.length){
            System.out.println(animals[y]);
            y++;
        }

        System.out.println();

        System.out.println("do...while loop:");
        int z = 0;
        do{
            System.out.println(animals[z]);
            z++;
        }while (z < animals.length);
        System.out.println();
        

        System.out.println();


    }

}


