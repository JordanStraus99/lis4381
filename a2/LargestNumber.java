import java.util.Scanner;

public class LargestNumber {
     public static void main(String[] args) {

        System.out.println("Devloper: Jordan Straus");
        System.out.println("Program evulates largest of two integers.");
        System.out.println("Note: Program does *not* check for non numeric character or non interger values.\n");

        int num1, num2;

         Scanner scnr = new Scanner(System.in);

        System.out.print("Enter first interger: ");
        num1 = scnr.nextInt();

        System.out.print("Enter second interger: ");
        num2 = scnr.nextInt();

        System.out.println();

        if (num1 == num2){
            System.out.println("\nInterger are equal.");
        }

        else if (num1 > num2){
            System.out.printf("%d is larger than %d%n", num1, num2);
        }
        else {
            System.out.printf("%d is larger than %d%n", num2, num1);
          

        
        }

    }
}