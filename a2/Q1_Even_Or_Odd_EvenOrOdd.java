import java.util.Scanner;
public class Q1_Even_Or_Odd_EvenOrOdd
{
    public static void main(String[] args)
    {
        
        System.out.println("Developer: Jordan Straus");
        System.out.println("Program eveluates intergers as even or odd.");
        System.out.println("Note:Program does *not* check for non-numeric characters.");
        System.out.println();
        
        int x = 0;
        System.out.print("Enter interger:");
        Scanner sc= new Scanner(System.in);
        x = sc.nextInt();
        
        if (x % 2 == 0)
        {
            System.out.println(x + " is an even number");
        }
        else 
        {
            System.out.println(x + " is an odd number. ");
                } 

        sc.close();
        
    }
             
}    