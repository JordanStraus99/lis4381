> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381- Mobile Web Application Development

## Jordan Straus

### Assignment 5 Requirements:

## Three Parts

1. Clone Assignment
2. Create Pet Store Procces 
4. Skill Sets

#### README.md file should inclue the following items

1. Failed Validation Assignment 5 Bootstrap
2. Passed Validation Assignment 5 Bootstrap
3. Link to Local web-app
4. Skill Sets 13-15


### Links To Local Web-App:
1. [Simple Calculator:](http://localhost/repos/lis4381/simple_calculator/ "Skill Set 14")
2. [Write and Read Files:](http://localhost/repos/lis4381/write_read_file/ "Skill Set 15")  


#### Assignment 5 Screenshots :

| *Screenshot of  A5 Petstore Invalid*:                   | *Screenshot of A5 Petstore Valid*:                 |
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Assignment 5:](img/ss3.png)                  | ![Assignment 5:](img/ss2.png)                  |                          



| *Screenshot of  Skill Set 13*:                   
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Skill Set 13:](img/13.png)                  



| *Screenshot of  Skill Set 14*:                   | *Screenshot of Skills Set 14*:                 |
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Skill Set 14:](img/14.1.png)                  | ![Skill Set 14:](img/14.2.png)                  |                          


| *Screenshot of  Skill Set 15*:                   | *Screenshot of Skills Set 15*:                 |
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Skill Set 15:](img/15.1.png)                  | ![Skill Set 15:](img/15.2.png)                  |                          