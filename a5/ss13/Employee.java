class Employee extends Person
{
    
    
    private int ssn;
    private char gender;

    public Employee()
    {
        super();
        System.out.println("\nInside employee default constructor.");
        ssn= 123456789;
        gender = 'X';
    }

    public Employee(String f, String l, int a, int s, char g)
    {
        super(f,l,a);
        System.out.println("\nInsdie employee constructor with parameters.");
        ssn = s;
        gender = g;
    }
    
    public int getSsn()
    {
        return ssn;
    }

    public void setSsn(int s)
    {
        ssn = s;
    }

    public char getGender()
    {
        return gender;
    }

    public void setGender(char g)
    {
        gender = g;
    }

    public void print()
    {
        super.print();
        System.out.println(",SSN:" + ssn + ",Gender:" + gender);
    }

}
