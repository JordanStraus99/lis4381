import java.util.Scanner;;

class EmployeeDemo
{
    public static void main(String[] args)
    {
        String fn = "";
        String ln = "";
        int a = 0;
        int s = 987654321;
        char g = 'Y';
        Scanner sc = new Scanner(System.in);


        System.out.print("\n//////Below are base class default constructor values://///");
        Person p1 = new Person();
        System.out.println("\nFname = " + p1.getFname());
        System.out.println("Lname = " + p1.getLname());
        System.out.println("Age = " + p1.getAge());
        
        System.out.print("\n/////Below are base class user-entered values://///");

        System.out.print("\nFname: ");
        fn = sc.nextLine();

        System.out.print("Lname: ");
        ln = sc.nextLine();
        
        System.out.print("Age:  ");
        a = sc.nextInt();
        
        Person p2 = new Person(fn, ln, a);
        System.out.println("\nFname = " + p2.getFname());
        System.out.println("Lname = " + p2.getLname());
        System.out.println("Age = " + p2.getAge());

        System.out.println("\n/////Below using setter methods to pass literal values,than print() method to display values://///");
        p2.setFname("Jordan");
        p2.setLname("Straus");
        p2.setAge(21);
        p2.print();

        System.out.println();

        System.out.print("\n/////Below are derived class default constructor values://///");
        Employee e1 = new Employee();
        System.out.println("\nFname = " + e1.getFname());
        System.out.println("Lname = " + e1.getLname());
        System.out.println("Age = " + e1.getAge());
        System.out.println("SSN = " + e1.getSsn());
        System.out.println("Gender = " + e1.getGender());

        System.out.println("\nOr...");
        e1.print();

        System.out.print("\nFname: ");
        fn = sc.next();

        System.out.print("Lname: ");
        ln = sc.next();

        System.out.print("Age: ");
        a = sc.next();

        System.out.print("SSN: ");
        s = sc.next();

        System.out.print("Gender: ");
        g = sc.next().charAt(0);

        Employee e2 = new Employee(fn, ln, a, s, g);
        System.out.println("\nFname = " + e2.getFname());
        System.out.println("Lname = " + e2.getLname());
        System.out.println("Age = " + e2.getAge());
        System.out.println("SSN = " + e2.getSsn());
        System.out.println("Gender = " + e2.getGender());

        System.out.println("\nOr...");
        e2.print();

    }

}