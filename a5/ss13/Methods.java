import java.util.Scanner;

public class Methods 
{
    public static void getRequirements()
    {
        System.out.println("Sphere Volume Program");
        System.out.println();
        System.out.println("Devloper: Jordan Straus");
        System.out.println("Program calculates spheres vloume in liquid U.S, gallons from user-entered diamerter value in inches, and rounds two decimal places.");
        System.out.println("Must use java's *built-in* PI and pow() capabilities.");
        System.out.println("Program checks for non-integers and non numeric values.");
        System.out.println("Program countines to prompt for user entry until no longer requested,prompt accepts upper or lower case letters.");
        System.out.println();
    }

    public static void getVolume()
    {
        final Scanner input = new Scanner(System.in);
        String response = "y";
        int num = 0;

        while (response.equals("y"))
        {
            System.out.print("please enter diameter in inches: ");
            while (!input.hasNextInt())
            {
                System.out.println("Not valid integer!");
                input.next();
                System.out.print("\nPlease try again. Enter diameter in inches");
            }
            num = input.nextInt();

            System.out.println("\nSphere volume: " + doConversion(num) + "liquid U.S. gallons");
            System.out.print("\nDo you want to calculate another sphere volume (y or n)? ");
            response = input.next().toLowerCase();
            input.nextLine();
        }
       System.out.println("\nThank you for using our Sphere Volume Calculator!");
    }
public static double calculateVolume(int diameter)
{
    double volume = 0.0;
    int radius = diameter / 2;
    double r3 = Math.pow(radius, 3);
    volume = (4.0/3.0) * (Math.PI) * r3;
    return volume;
}

public static String doConversion(int diameter)
{
    double volume = calculateVolume(diameter);
    double conversion = 0.00432900433;
    double gallons = volume * conversion;
    String myStr = String.format("%.2f", gallons);
    return myStr;
}
    

}
