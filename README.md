# LIS4381 Web Application Devlopent 

## Jordan Straus


Course Links

1. [Assignment 1](a1/README.md "MY A1 README.md file")

    - Install AMPPS
    - Install JDK
    - Install Android Studio and Create My First App
    - Create Bitbucket repo
    - Complete Bitbucket Tutorials (bitbucketstationlocations and myteamqoutes)
    - Provide git command desription
    - Provide screenshots of installations

2. [Assignment 2](a2/README.md"MY A2 README.md file")

      - Create Healthy Recipes Android App
      - Provide Screenshots of completed App
      - Provide Screenshots of completed Java Skill Sets

3. [Assignment 3](a3/README.md "MY A3 README.md file")

      - Create Concert App
      - Provite Screenshot Running Application of 1st user interface
      - Provite Screenshot Running Application of 2nd user interface
      - Complete and Provide Screenshots of Skill Set 4-5-6
      - Screenshot of ERD

4. [Project 1](p1/README.md "MY p1 README.md file")
    - Screenshot of running application’s first user interface
    - Screenshot of running application's second user interface
    -  Skill Sets 7-8-9

5. [Assignment 4](a4/README.md "MY A4 README.md file")

      - LIS4381 Portal Bootstrap
      - Failed Validation Bootstrap
      - Passed Validation Bootstrap
      - Link to Local web-app
      - Skill Sets 10-12

6. [Assignment 5](a5/README.md "MY A5 README.md file")

      - LIS4381 Portal Bootstrap
      - A5 Pet Store index.php Passed
      - A5 Pet Store index.php Error
      - Link to Local web-app
      - Skill Sets 13-15

      
7. [Project 2](p2/README.md "MY p2 README.md file")

      - LIS4381 Portal Bootstrap
      - A5 Pet Store Server Validation Passed
      - A5 Pet Store Server Validation Error
      - Link to Local web-app
      - RSS Feed


