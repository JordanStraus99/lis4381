> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381- Mobile Web Application Development

## Jordan Straus

### Assignment 1 Requirements:

1. Distributed Version Control Setup:Part 2 Development Installation:
2. AMPPS: (http://www.ampps.com/)
3. Tour: http://www.ampps.com/tour
4. Download and Installation: http://www.ampps.com/wiki/Main_Page
5. Installation Demo: http://www.ampps.com/demo
6. Java: If necessary, download and install JDK:
7. https://www.ntu.edu.sg/home/ehchua/programming/howto/JDK_HowTo.html 
8. Android Studio (download and install): http://developer.android.com/sdk/index.html
9. Building Your First App: http://developer.android.com/training/basics/firstapp/index.html 
10. Push your local repository to the one hosted by Bitbucket's servers: See Part 1 (above).
11. Provide me with read-only access to Bitbucket repository: See Part 1 (above).

#### README.md file should include the following items:

1. Screenshot of ampps installation running (#1 above);
2. Screenshot of running JDK java Hello (#2 above);
3. Screenshot of running Android Studio My First App (#3 above, example below); (Note: use *your name* instead of “Hello World!”)
4. git commands w/short descriptions (“Lesson 3b - Version Control Systems: Course Configuration”);
5. Bitbucket repo links:

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Git commands w/short descriptions:

* git init - initializes a new empty repository on your local machine:

* git status -  displays the state of a working command

* git add - adds all files in the current directory and all sub directories to your local working directory.

* git commit - commits new and updated files

* git push - gathers all the committed files from your local repository and uploads them to a remote repository

* git pull - literally pulls the changes made from a remote repository to your local repository. 

* git log -  is used to view the history of your repository.

#### Assignment 1 Screenshots:

*Screenshot of AMPPS running http://localhost*:
![JDK Installation Screenshot](img/amp.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/JordanStraus99/bitbucketstationlocation/src/master/ "Bitbucket Station Locations")

