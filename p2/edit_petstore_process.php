<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//exit(print_r($_POST)); //display $_POST array values from form

// or, for nicer display in browser...
//echo "<pre>";
//print_r($_POST);
//echo "</pre>";
//exit(); //stop processing, otherwise, errors below


//code to process inserts goes here
$pts_id_v = $_POST['pts_id'];
$pts_name_v = $_POST['name'];
$pts_street_v = $_POST['street'];
$pts_city_v = $_POST['city'];
$pts_state_v = $_POST['state'];
$pts_zip_v = $_POST['zip'];
$pts_phone_v = $_POST['phone'];
$pts_email_v = $_POST['email'];
$pts_url_v = $_POST['url'];
$pst_ytd_sales_v = $_POST['ytdsales'];
$pts_notes_v = $_POST['notes'];


$pattern='/^[a-zA-Z0-9\-_\s]+$/';
$valid_name = preg_match($pattern, $pts_name_v);

$pattern='/^[a-zA-Z0-9,\-\s.]+$/';
$valid_street = preg_match($pattern, $pts_street_v);

$pattern='/^[a-zA-Z0-9\s]+$/';
$valid_city = preg_match($pattern, $pts_city_v);

$pattern='/^[a-zA-Z]+$/';
$valid_state = preg_match($pattern, $pts_state_v);

$pattern='/^\d{5,9}+$/';
$valid_zip = preg_match($pattern, $pts_zip_v);

$pattern='/^\d{10}+$/';
$valid_phone = preg_match($pattern, $pts_phone_v);

$pattern='/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
$valid_email = preg_match($pattern, $pts_email_v);

$pattern='/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
$valid_url = preg_match($pattern, $pts_url_v);

$pattern='/^[0-9\.]+$/';
$valid_ytd = preg_match($pattern, $pts_ytd_sales_v);


if (
    empty($pts_name_v) ||
    empty($pts_street_v) ||
    empty($pts_city_v) ||
    empty($pts_state_v) ||
    empty($pts_zip_v) ||
    empty($pts_phone_v) ||
    empty($pts_email_v) ||
    empty($pts_url_v) ||
    !isset($pts_ytd_sales_v) 
)
{
    $error = "All fields require data, except Notes. Check all fields and try again";
    include('global/error.php');
}

else if (!is_numeric($pts_ytd_v) || $pts_ytd_v <= 0)
{
    $error = "YTD sales can only contain numbers (other than a decimal point) and must be greater than or equal to 0";
    include('global/error.php');
}

else if ($valid_name === false)
{
    echo 'Error in pattern!';
}

else if ($valid_name === 0)
{
    $error = "'Name can only contain letters, numbers, hyphens, and underscore";
    include('global/error.php');
}

else if ($valid_street === false)
{
    echo 'Error in pattern!';
}

else if ($valid_street === 0)
{
    $error = "Street can only contain letters, numbers, commas, hyphens, or periods";
    include('../global/error.php');
}

else if ($valid_city === false)
{
    echo 'Error in pattern!';
}

else if ($valid_city === 0)
{
    $error = "City can only contain letters, numbers, commas, hyphens, or periods";
    include('../global/error.php');
}

else if ($valid_state === false)
{
    echo 'Error in pattern!';
}

else if ($valid_state === 0)
{
    $error = "State can only contain 2 letters";
    include('../global/error.php');
}

else if ($valid_zip === false)
{
    echo 'Error in pattern!';
}

else if ($valid_zip === 0)
{
    $error = "Zip must be 5-9 digits with no other characters";
    include('../global/error.php');
}

else if ($valid_phone === false)
{
    echo 'Error in pattern!';
}

else if ($valid_phone === 0)
{
    $error = "Phone must be 10 digits with no other characters";
    include('../global/error.php');
}

else if ($valid_email === false)
{
    echo 'Error in pattern!';
}

else if ($valid_email === 0)
{
    $error = "Must be valid email address";
    include('../global/error.php');
}

else if ($valid_url === false)
{
    echo 'Error in pattern!';
}

else if ($valid_url === 0)
{
    $error = "Must be valid URL";
    include('../global/error.php');
}

else if ($valid_ytd === false)
{
    echo 'Error in pattern!';
}

else if ($valid_ytd_sales === 0)
{
    $error = "YTD sales must contain no more than 10 digits, including decimal point";
    include('../global/error.php');
}

else 
{
    require_once('../global/connection.php');

    $query = 
    "UPDATE petstore 
    SET
    pts_name = :pts_name_p,
    pts_street = :pts_street_p,
    pts_city = :pts_city_p,
    pts_state = :pts_state_p,
    pts_zip = :pts_zip_p,
    pts_phone = :pts_phone_p,
    pts_email = :pts_email_p,
    pts_url = :pts_url_p,
    pts_sales = :pts_ytd_p,
    pts_notes = :pts_notes_p
    WHERE pts_id = :pts_id_p";

exit($query);

try
{
$statement = $db->prepare($query);
$statement->bindParam(':pts_id_p', $pts_id_v);
$statement->bindParam(':pts_name_p', $pts_name_v);
$statement->bindParam(':pts_street_p', $pts_street_v);
$statement->bindParam(':pts_city_p', $pts_city_v);
$statement->bindParam(':pts_state_p', $pts_state_v);
$statement->bindParam(':pts_zip_p', $pts_zip_v);
$statement->bindParam(':pts_phone_p', $pts_phone_v);
$statement->bindParam(':pts_email_p', $pts_email_v);
$statement->bindParam(':pts_url_p', $pts_url_v);
$statement->bindParam(':pts_ytd_p', $pts_ytd_v);
$statement->bindParam(':pts_notes_p', $pts_notes_v);
$row_count = $statement->execute();
$statement->closeCursor();

$last_auto_increment_id = $db->lastInsertId();


//include('index.php'); //forwarding is faster, one trip to server
header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
}//end try

catch (PDOException $e)
{
    $error = $e->getMessage();
    echo $error;
}
}
?>
