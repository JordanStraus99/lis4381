> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381- Mobile Web Application Development

## Jordan Straus

### Project 2 Requirements:

## Three Parts

1.Online Portfolio Link
2. Pet Store Edit/Delete 
4. Chapter Questions

#### README.md file should inclue the following items

1. Failed Server-Side Validation Project 2 5 
2. Passed Server-Side Validation Project 2 
3. Link to Local web-app
4. RSS feed


### Links To Local Web-App:
1. [Online Portfolio:](http://localhost/repos/lis4381/ "Online Portfolio")



#### Project 2 Screenshots :
| *Project 2 Screen*:                    |
| ------------------------------------------------ |
|![P2 Petstore:](img/p2.png)                   |



| *Screenshot of  P2 Petstore Invalid*:                   | *Screenshot of P2 Petstore Valid*:                 |
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Project 2:](img/p4.png)                  | ![Project 2:](img/p5.png)                  |                          


| *Carousel Home Page:*:                    |
| ------------------------------------------------ |
|![Carosel:](img/p1.png)                   |


| *Screenshot of  RSS Feed*:                    |
| ------------------------------------------------ |
|![RSS:](img/p3.png)                   |