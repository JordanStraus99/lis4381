> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381- Mobile Web Application Development

## Jordan Straus

### Project 1 Requirements:

## Two Parts

1. My Business Card Mobile App
2. Java Skill Sets 7-9

#### README.md file should inclue the following items

1. Screenshots of running mobile app
2. Screenshots of Java Skill Sets

#### Project 1 Screenshots :

| *Screenshot of My Business Card*:       | *Screenshot of My Business Card*:   |
| ------------------------------------------------ | -------------------------------------------- |
|![Screenshot of Application](imgg/BC2.png)      | ![Screenshot of Application](imgg/BC1.png) |





| *Screenshot of  Skill Set 7*:                   | *Screenshot of Skill Set 8*:                 |
| ----------------------------------------------- |-----------------------------------------------|                                                    
|![Skill Set 7:](imgg/Skill7.png)                  |![Skill Set 8:](imgg/SK*.png)                  |                          



| *Screenshot of  Skill Set 9*:                    |
| ------------------------------------------------ |
|![Skill Set 3:](imgg/Skill9.png)                   |